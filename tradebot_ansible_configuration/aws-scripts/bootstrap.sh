# Install Python-Anaconda
sudo yum install bzip2 wget
wget https://repo.continuum.io/archive/Anaconda3-5.1.0-Linux-x86_64.sh

ANACONDA_PATH=/opt/anaconda3
sudo mkdir -p $ANACONDA_PATH
sudo chown -R $USER:$USER $ANACONDA_PATH
bash Anaconda3-5.1.0-Linux-x86_64.sh -b -f -p $ANACONDA_PATH

echo "export ANACONDA_PATH=$ANACONDA_PATH" >> ~/.bashrc

# Install commonly used conda packages
/opt/anaconda3/bin/conda install python-snappy

# Upgrade pip
/opt/anaconda3/bin/pip install -U pip

# Dev Stack
/opt/anaconda3/bin/pip install -U \
  jupyterlab s3fs pyarrow fastparquet pymysql sqlalchemy \
  ipywidgets

# Data Sci packages
/opt/anaconda3/bin/pip install -U \
  pandas numpy dask framequery scikit-learn findspark \
  matplotlib seaborn bokeh
