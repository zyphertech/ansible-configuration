This repository is a repository that contains ansible roles which are used by lendcypher team to manange infrastructure.

## Installation
1. Install Python
1. Install ansible by running `pip install ansible`
1. Install roles based dependencies by running `ansible-galaxy install -r requirements.yml`

## Roles
- ansible-redis: role to deploy redis/redis-custer works on both single node and multinode configuration
- ansible-role-selenium: Setup selenium with either chrome/firefox - by Alex Knoll
- aws_cli: Setup aws-cli
- backend: role to deploy backend repo
- crontab: configures crontab for applicaiton server and sets timezone
- datapull: deploy the datapull repo, create virtualenv, and create cron
- frontend: role to deploy frontend repo
- github: Setup github SSH key
- mongodb: role to deploy mongodb and setup the database named LC
- nginx: role to configure nginx on server
- update_datapull: role to update the datapull repo

## Running a playbook

``` bash
ansible-playbook -i hosts <path_to_playbook>
```

## Backend setup
 1. First copy the SSL keys into `/ssl/host.cert` and `/ssl/host.key`
 1. Change permissions to 644 for both the files
 1. Run the playbook: `playbooks/deploy_backend.yml`

For staging we use:
 - `ansible-playbook -i hosts --extra-vars="port=$port target_hosts=staging_server branch=$CIRCLE_BRANCH github_ssh_key=~/.ssh/id_aws-application-server" playbooks/deploy_backend.yml`

For production we use:
 - `ansible-playbook --private-key="~/.ssh/NEWCENTOS.pem" -i hosts --extra-vars='{ branch: "master", env: "prod", subdomain: False }'  playbooks/deploy_backend.yml`

## Frontend setup
 1. First copy the SSL keys into `/ssl/host.cert` and `/ssl/host.key`
 1. Change permissions to 644 for both the files
 1. Run the playbook: `playbooks/deploy_frontend.yml`

For staging we use:
 - `ansible-playbook -i hosts --extra-vars="port=$port target_hosts=staging_server branch=$CIRCLE_BRANCH github_ssh_key=~/.ssh/id_aws-application-server" playbooks/deploy_backend.yml`

For production we use:
 - `ansible-playbook --private-key="~/.ssh/NEWCENTOS.pem" -i hosts --extra-vars='{ branch: "master", env: "prod", subdomain: False }'  playbooks/deploy_frontend.yml`


Note: We use JSON as False (the bool) can be passed in
JSON only. Refer https://github.com/ansible/ansible/issues/17193


ansible-playbook --private-key="~/.ssh/NEWCENTOS.pem" -i hosts --extra-vars='{ branch: "master", env: "prod", subdomain: False, github_ssh_key=~"~/.ssh/bitbucket_zypher" }'  playbooks/deploy_frontend.yml

ansible-playbook --private-key="~/.ssh/NEWCENTOS.pem" -i hosts --extra-vars='{ branch: "master", env: "prod", subdomain: False, github_ssh_key=~"~/.ssh/bitbucket_zypher" }'  playbooks/deploy_backend.yml
